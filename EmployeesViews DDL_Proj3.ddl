-- Creates view for the original tables so that they cannot be altered manually, only through the views
DROP VIEW Company_view;
DROP VIEW Non_profit_view;
DROP VIEW For_profit_view;

-- Create a view for Company table
create view Company_view as
	SELECT
		Company_ID,
		Company_Name,
		type
	FROM Company WHERE type = 'Company';

-- Trigger for Company values to go into Company table
	create OR replace TRIGGER Company_trigger
		INSTEAD OF insert ON Company_view
		FOR EACH ROW
	BEGIN
		INSERT INTO Company(
			Company_ID,
			Company_Name,
			type)
		VALUES (
			:NEW.Company_ID,
			:NEW.Company_Name,
			'Company');
	END;
	/

--Create view for Non-Profit Table 
create view Non_Profit_view as
	SELECT
		Company_ID,
		Company_Name,
		type,
		Year_Opened
	FROM Company where type = 'Non_Profit';

-- Create trigger for view values to go into original Non_Profit Table
	create or replace TRIGGER Non_profit_trigger
		INSTEAD OF insert ON Non_Profit_view
		FOR EACH ROW
	BEGIN
		insert into Company(
			Company_ID,
			Company_Name,
			type,
			Year_Opened)
		VALUES (
			:NEW.Company_ID,
			:NEW.Company_Name,
			'Non_Profit',
			:NEW.Year_Opened
			);
	END;
	/

-- Create view for For_Profit table
create view For_profit_view as
		SELECT
			Company_ID,
			Company_Name,
			type,
			Total_Revenue
		FROM Company where type = 'For_Profit';
		
-- Creates trigger to replace values into original For_profit table
		create or replace TRIGGER For_profit_trigger
			INSTEAD OF insert ON For_profit_view
			FOR EACH ROW
		BEGIN
			insert into Company(
				Company_ID,
				Company_Name,
				type,
				Total_Revenue)
			VALUES (
				:NEW.Company_ID,
				:NEW.Company_Name,
				'For_Profit',
				:NEW.Total_Revenue) ;
		END;
		/

